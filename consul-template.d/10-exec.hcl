exec {
  command = "/usr/sbin/nginx -g 'daemon off;'"
  reload_signal = "SIGHUP"
  kill_signal = "SIGQUIT"
}
