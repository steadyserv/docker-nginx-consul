# nginx-consul Docker Image

An extendable docker image that runs Nginx with its configuration driven by
Consul via [Consul-Template][1].

This is meant to act as a base image - actual Nginx configuration should
be managed in derived images with config files and/or consul templates.

## Environment variables
You must set `CONSUL_HTTP_ADDR` in order for consul-template to render
any templates when the container runs. Any supported consul-template environment
variables can be used, such as:

- CONSUL_HTTP_ADDR
- CONSUL_HTTP_TOKEN
- CONSUL_HTTP_AUTH
- CONSUL_HTTP_SSL
- CONSUL_HTTP_SSL_VERIFY

See the consul-template [documentation][1] for details about these settings.

Additionally, the following environment variables can be used to adjust
consul-template:

- NGINX_CONF_TEMPLATE - path to nginx.conf template,
  defaults to `/etc/nginx/nginx.conf.ctmpl`
- NGINX_TEMPLATE_DIR - path for conf.d templates to be rendered to
  /etc/nginx/conf.d, defaults to `/etc/nginx/templates.d`
- CTMPL_LOG_LEVEL - defaults to `err`

## Customizing Nginx
By default, the [upstream nginx base image][2] contains a default global/http
configuration in `/etc/nginx/nginx.conf` and server (virtual host)
configuration in `/etc/nginx/conf.d/default.conf`. In most cases neither of
these are sufficient and should be overwritten.

If the file `${NGINX_CONF_TEMPLATE}` exists at runtime, it will be registered
as a consul template and rendered to `/etc/nginx/nginx.conf`,
effectively overwriting the default main configuration. You can also override
the default configuration without a template by simply adding your own
`/etc/nginx/nginx.conf` in a derived image.

### Site Configuration(s)
Any files contained in the `${NGINX_TEMPLATE_DIR}` directory that end in `.ctmpl`
will be registered as a consul template at runtime. The `.ctmpl` extension
will be stripped and rendered to the `/etc/nginx/conf.d/` directory.

For example, `${NGINX_TEMPLATE_DIR}/default.conf.ctmpl` will be rendered to
`/etc/nginx/conf.d/default.conf`.

Additional Nginx customizations can be made by adding `.conf` files to
`/etc/nginx/conf.d` (these files will not be rendered by consul-template).

For additional control over the configuration of consul-template, valid HCL
or JSON consul-template config(s) can be added to `/etc/consul-template.d/`.

[1]: https://github.com/hashicorp/consul-template
[2]: https://hub.docker.com/_/nginx/
