#!/bin/sh
set -e

: ${NGINX_CONF_TEMPLATE="/etc/nginx/nginx.conf.ctmpl"}
: ${NGINX_TEMPLATE_DIR="/etc/nginx/templates.d"}
: ${CTMPL_LOG_LEVEL="err"}

ARGS="-config=/etc/consul-template.d"
ARGS="${ARGS} -log-level=${CTMPL_LOG_LEVEL}"

if [ -f "${NGINX_CONF_TEMPLATE}" ]; then
  ARGS="${ARGS} -template=${NGINX_CONF_TEMPLATE}:/etc/nginx/nginx.conf"
fi

if [ -d "${NGINX_TEMPLATE_DIR}" ]; then
  for tmpl in ${NGINX_TEMPLATE_DIR}/*.ctmpl; do
    ARGS="${ARGS} -template=${tmpl}:/etc/nginx/conf.d/$(basename $tmpl .ctmpl)"
  done
fi

# Test the nginx config
if [ "$1" = "-t" ]; then
  shift
  exec consul-template -exec="/usr/sbin/nginx -t" $ARGS "$@" || exit 1
fi

exec consul-template $ARGS "$@"
